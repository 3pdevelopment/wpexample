jQuery(document).ready(function () {

    jQuery(document).on('submit', '#task_form', function (event) {
        event.preventDefault();
        
        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                'action': 'ajax_form',
                'full_name': jQuery('#full_name').val()
            },
            beforeSend: function () {
//                console.log("Before");
            },
            success: function (data) {
                var apiData = jQuery("#api_data").html("");
                var obj = JSON.parse(data);
                
                for(var key in obj) {
                    apiData.append("<div>"+key+": "+obj[key]+"</div>");
                 }
            },
            error: function () {
                console.log("Error");
            },
        });
    });

});