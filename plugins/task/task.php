<?php
/*
   Plugin Name: Task plugin
   Plugin URI: http://3pdevelopment.com
   Description: Custom plugin developed for task requirements
   Version: 1.1
   Author: Mile Janev
   Author URI: http://3pdevelopment.com
   License: GPL2
*/

add_action('admin_menu', 'task_plugin_setup_menu');
 
function task_plugin_setup_menu(){
    add_menu_page( 'Task Plugin', 'Task Plugin', 'manage_options', 'task', 'task_init' );
}

function task_scripts() {
    wp_enqueue_script('task', plugin_dir_url( __FILE__ ).'task.js' , ['jquery'], false, true);
}
add_action('admin_enqueue_scripts', 'task_scripts');
 
function task_init(){
    $full_name = get_option('full_name');
    
    task_handle_post();
?>
    <h2>Full Name</h2>
    <form method="post" name="task_form" id="task_form">
        <input type="text" id="full_name" name="full_name" value="<?= $full_name ?>" />
        <?php submit_button('Save') ?>
    </form>
    <div id="api_data"></div>
<?php
}
 
function task_handle_post(){
    if (isset($_POST['full_name'])) {
        update_option('full_name', $_POST['full_name'], '', 'yes' );
    }
}


function ajax_form(){
    if (isset($_POST['full_name'])) {
        update_option('full_name', $_POST['full_name'], '', 'yes' );
    }
    
    //API call
    $response = wp_remote_get('https://orbisius.com/apps/qs_cmd/?json');
    $body = wp_remote_retrieve_body( $response );
    echo $body;
    
    exit();

}
add_action('wp_ajax_ajax_form', 'ajax_form');
add_action('wp_ajax_nopriv_ajax_form', 'ajax_form');