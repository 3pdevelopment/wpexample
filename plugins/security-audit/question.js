$(document).ready(function(){
    
    $("#pdf-submit").click(function(e){
        $("#send-pdf-button").click();
    });
    
    var barPosition = 0;
    var percentile = parseFloat( $.trim($("#question-percentile").html()) );
    $('body').on('click', '#question-form .answer', function(e) {
        
        var $this = $(this);
        
        $("#question-wrapper").animate({ opacity: 0 }, 300, function(){
            
            var post_id = $("#post_id").val();
            var answer = $this.val();
            
            var menu_order = $("#menu_order_id").val();

            $.ajax({
                    url : postquestion.ajax_url,
                    type : 'post',
                    data : {
                        action : 'post_question',
                        post_id : post_id,
                        answer: answer,
                        menu_order: menu_order
                    },
                    success : function( response ) {
                        if (response.status == "ok") {
                            var question = response.question;
                            if (question != false) {
                                $(".answer").prop('checked', false);//Uncheck radio buttons for new question
                                $("#post_id").val(question.id);
                                $("#question-category").html(question.category);
                                $("#question-subcategory").html(question.subcategory);
                                $("#question-content").html(question.title);
                                $("#menu_order_id").val(question.menu_order);

                                $("#question-wrapper").animate({ opacity: 1 }, 300);

                                //Following code for Progressbar START
                                barPosition += percentile;
                                barPosition = Math.round(barPosition * 10000) / 10000;
                                $(".ui-widget-header").css("width", barPosition + "%");
                                //Check checboxes if needs
                                $( ".check-button.unchecked" ).each(function( index ) {
                                    var checkboxPosition = $(this).attr("data-position");
                                    if (barPosition >= checkboxPosition) {
                                        $(this).removeClass("unchecked");
                                    }
                                });
                                //Following code for Progressbar END

                            } else {
                                //Following code for Progressbar START
                                barPosition += percentile;
                                barPosition = Math.round(barPosition * 10000) / 10000;
                                $(".ui-widget-header").css("width", barPosition + "%");
                                //Check checboxes if needs
                                $( ".check-button.unchecked" ).each(function( index ) {
                                    var checkboxPosition = $(this).attr("data-position");
                                    if (barPosition >= checkboxPosition) {
                                        $(this).removeClass("unchecked");
                                    }
                                });
                                //Following code for Progressbar END
                                window.location.href = $("#logo").attr("href") + "/result";
                            }
                        }
                    }
            });
            
        });
        
        
    });
    
    
    
})