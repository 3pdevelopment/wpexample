<?php
/*
   Plugin Name: Security Audit
   Plugin URI: http://yanev.com
   Description: Custom plugin for Security Audit test
   Version: 1
   Author: Mile Janev
   Author URI: http://yanev.com
   License: GPL2
*/
use Dompdf\Dompdf;

//Database Table Generate START
global $result_db_version;
$result_db_version = '1.0';
function result_install() {
	global $wpdb;
	global $result_db_version;

	$table_name = $wpdb->prefix . 'result';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		email varchar(127) NOT NULL,
		pdf_link varchar(127) NOT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'result_db_version', $result_db_version );
}
register_activation_hook( __FILE__, 'result_install' );
//Database Table Generate END


add_action('admin_menu', 'my_menu_pages');
function my_menu_pages(){
    add_menu_page('PDFs', 'PDFs', 'manage_options', 'pdfs', 'administrate_pdfs' );
}

//Function for viewing generated pdfs START
function administrate_pdfs() {
?>
<h1>Generated PDFs</h1>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Email</th>
            <th>PDF link</th>
            <th>Time</th>
        </tr>
    </thead>
    <tbody>
<?php
    global $wpdb;
    $table_name = $wpdb->prefix . 'result';
    
    $results = $wpdb->get_results("SELECT * FROM ".$table_name." ORDER BY id DESC");
?>
    <?php foreach ($results as $result) { ?>
        <tr>
            <td><?= $result->id ?></td>
            <td style="padding: 5px 30px;"><?= $result->email ?></td>
            <td style="padding: 5px 30px;"><a target="_blank" href="<?= esc_url(home_url('/')) . "pdfs/" . $result->pdf_link ?>"><?= $result->pdf_link ?></a></td>
            <td style="padding: 5px 30px;"><?= $result->time ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php
}
//Function for viewing generated pdfs END

function question_init() {
    $labels = array(
        'name' => 'Question',
        'singular_name' => 'Question',
        'menu_name' => 'Questions',
        'name_admin_bar' => 'Question',
        'archives' => 'Question Archives',
        'parent_item_colon' => 'Parent Question:',
        'all_items' => 'All Questions',
        'add_new_item' => 'Add New Question',
        'add_new' => 'Add New Question',
        'new_item' => 'New Question',
        'edit_item' => 'Edit Question',
        'update_item' => 'Update Question',
        'view_item' => 'View Question',
        'search_items' => 'Search Questions',
        'not_found' => 'Not found',
        'not_found_in_trash' => 'Not found in Trash',
        'featured_image' => 'Featured Image',
        'set_featured_image' => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image' => 'Use as featured image',
        'insert_into_item' => 'Insert into Question',
        'uploaded_to_this_item' => 'Uploaded to this Question',
        'items_list' => 'Questions list',
        'items_list_navigation' => 'Questions list navigation',
        'filter_items_list' => 'Filter Questions list',
    );
    $args = array(
        'label' => 'Question',
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'question'),
        'query_var' => true,
        'taxonomies' => ['question_category'],
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
        )
    );
    register_post_type('question', $args);
}
add_action('init', 'question_init');

function question_category(){
    //set the name of the taxonomy
    $taxonomy = 'question_category';
    //set the post types for the taxonomy
    $object_type = 'question';
    
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'Question Category',
        'singular_name'      => 'Question Category',
        'search_items'       => 'Search Question Categories',
        'all_items'          => 'All Question Categories',
        'parent_item'        => 'Parent Question Category',
        'parent_item_colon'  => 'Parent Question Category:',
        'update_item'        => 'Update Question Category',
        'edit_item'          => 'Edit Question Category',
        'add_new_item'       => 'Add New Question Category', 
        'new_item_name'      => 'New Question Category Name',
        'menu_name'          => 'Categories'
    );
    
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'question_category')
    );
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init', 'question_category');

//Create custom fields for this post type
function question_fields_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
    <div>
        <label for="meta-box-note">Note: </label><br />
        <textarea name="meta-box-note" style="width: 100%"><?php echo get_post_meta($object->ID, "meta-box-note", true); ?></textarea>
    </div>
<?php  
}
function add_question_meta_box()
{
    add_meta_box("question-meta-box", "Additional Parameters", "question_fields_markup", "question", "normal", "high", null);
}
//add_action("add_meta_boxes", "add_question_meta_box");//Uncomment this to add back "Note" field

function save_question_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;
    
    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
    
    $slug = "question";
    
    if($slug != $post->post_type) {
        return $post_id;
    }

    if(isset($_POST["meta-box-note"])){
        $meta_box_note_value = $_POST["meta-box-note"];
    } else {
        $meta_box_note_value = "";
    }
    
    update_post_meta($post_id, "meta-box-note", $meta_box_note_value);
}
add_action("save_post", "save_question_meta_box", 10, 3);

function remove_question_meta_box()
{
    remove_meta_box("meta-box-note", "question", "normal");
}
add_action("do_meta_boxes", "remove_question_meta_box");
//Do tuka

add_action( 'wp_enqueue_scripts', 'ajax_question_enqueue_scripts' );
function ajax_question_enqueue_scripts() {
    wp_enqueue_script('question', plugins_url('/question.js', __FILE__), array('jquery'), '1.0', true);

    wp_localize_script('question', 'postquestion', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action( 'wp_ajax_nopriv_post_question', 'post_question' );
add_action( 'wp_ajax_post_question', 'post_question' );

//Ajax Action
function post_question() {
    session_start();
    
    header('Content-Type: application/json');
    
    $post_id = $_POST['post_id'];
    $answer = $_POST['answer'];
    $menu_order = $_POST['menu_order'] + 1;
    
    $_SESSION['questions'][$post_id] = $answer;
    
    $question = get_question($menu_order);
    
    $output = ["status" => "ok", "question" => $question];
    
    echo json_encode($output);
    die();
}

//Calculate percentile for one question
function get_question_percentile()
{
    $percentile = 0;
    
    $questionsNum = wp_count_posts('question');
    
    $percentile = round ( (100 / $questionsNum->publish), 4);
    
    return $percentile;
}

function get_question($menu_order = 1)
{
    $question = FALSE;
    $questions = get_posts(array(
        'post_type' => 'question',
        'order_by' => 'menu_order',
        'order' => 'ASC',
        'numberposts' => 1,
        'menu_order' => $menu_order
    ));
    
    if (!empty($questions)) {
        $question['menu_order'] = $menu_order;
        $question['id'] = $questions[0]->ID;
        $question['title'] = $questions[0]->post_title;
        $question['content'] = $questions[0]->post_content;
        
        //Categories START
        $categories = get_the_terms($question['id'], 'question_category');
        if (isset($categories[0])) {
            $question['category'] = $categories[0]->name;
        } else {
            $question['category'] = "";
        }
        if (isset($categories[1])) {
            $question['subcategory'] = $categories[1]->name;
        } else {
            $question['subcategory'] = "";
        }
        //Categories END
        
    }
    
    return $question;
}

function generate_pdf()
{
    session_start();
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'result';
    
    //Set path
    $ds = DIRECTORY_SEPARATOR;
    $projectRoot = get_theme_root().$ds."..".$ds."..".$ds;
    $pdf_directory = $projectRoot . 'pdfs' . $ds;
    if (!file_exists($pdf_directory)) {
        mkdir($pdf_directory, 0777);
    }
    
    require_once(get_template_directory().$ds.'dompdf'.$ds.'autoload.inc.php');
    require_once(get_template_directory().$ds.'PHPMailer'.$ds.'PHPMailerAutoload.php');
    
    $email = $_POST["email"];
    
    $html = "";
    
    $i = 1;
    foreach ($_SESSION['questions'] as $id => $answer) {
        $post = get_post($id);
        $result = calculate();
//        $post_note = get_post_meta($id, "meta-box-note", true);
        if ($answer == '1') {
            $answerSpan = "<span style='color: #0000ff'>Yes</span>";
        } else if ($answer == '0') {
            $answerSpan = "<span style='color: #ff0000'>No</span>";
        } else {
            $answerSpan = "<span style='color: #ffcc00'>Don`t know</span>";
        }
        
        $html .= "<p>" . $i . ") " . $post->post_title . " " . $answerSpan . "</p>";
        
        if ($answer != "1") {
            $html .= "<p style='color: #0000ff'> - " . $post->post_content . "</p>";
        }
        
        $html .= "<br />";
        
        $i++;
    }
    
    $html .= "<br /><br />";
    $html .= "<p><h1>Total: " . $result['total'] . "</h1></p>";
    
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $dompdf->setPaper('A4', 'portrait');
    $dompdf->render();
    
    $font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
    $dompdf->getCanvas()->page_text(72, 18, "Iron Clad Report: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
    
    $output = $dompdf->output();
    
    $pdf_name = pdf_find_name($email);
    file_put_contents($pdf_directory.$pdf_name, $output);
    
    //Insert into DB
    $wpdb->insert( 
        $table_name, 
        array(
            'time' => current_time( 'mysql' ), 
            'email' => $email, 
            'pdf_link' => $pdf_name, 
        ) 
    );
    
    
    //Send to email START
    $message = "";
    $message .= "Your Iron Clad Cyber Security report is attached below.<br /><br />";
    $message .= "Kind Regards,<br />";
    $message .= "Iron Clad Team";
    
    $emailMessage = new PHPMailer();
    $emailMessage->setFrom('no-reply@ironclad.com', 'Iron Clad');
    $emailMessage->Subject = 'IronClad Cyber Security Report';
    $emailMessage->Body = $message;
    $emailMessage->isHTML(true);  
    $emailMessage->AddAddress($email);
    
    $emailMessage->AddAttachment($pdf_directory.$pdf_name, $pdf_name, 'base64', 'application/pdf');
    $sent = $emailMessage->Send();
    //Send to email END
}

//Find PDF name from email
function pdf_find_name($email)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'result';
    
    $email_name_array = explode("@", $email);
    $pdf_name = $email_name_array[0] . ".pdf"; 
    $results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE pdf_link='".$pdf_name."'");
    
    if (!empty($results)) {
        $version = sprintf("%05d", 1);
        while (!empty($results)) {
            $pdf_name = $email_name_array[0] . "_" . $version . ".pdf";
            $results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE pdf_link='".$pdf_name."'");
            $version = sprintf("%05d", $version+1);
        }
    }
    
    return $pdf_name;
}

/**Returns:
 * Total number of answered questions
 * Total points
 * Total number of questions answered with 'Yes'
 * Total number of questions answered with 'No'
 * Total number of questions answered with 'Don`t know'
 * @return array
 */
function calculate()
{
    session_start();
    $questions = $_SESSION['questions'];
    
    $result = [
        'questions_number' => count($questions),
        'total' => 0,
        'yes' => 0,
        'no' => 0,
        'dont_know' => 0
    ];
    
    foreach ($questions as $id => $answer) {
        if ($answer == '1') {
            $result['total'] += 5;
            $result['yes']++;
        } else if ($answer == '0') {
            $result['no']++;
        } else if ($answer == '2') {
            $result['dont_know']++;
        }
    }
    
    $result['percents'] = ( $result['yes'] / $result['questions_number'] ) * 100;
    $result['color'] = get_chart_color($result['percents']);
    $result['label'] = get_chart_label($result['percents']);
    
    return $result;
}

//Get Chart color START
function get_chart_color($percent)
{
    if ($percent < 65) {
        $color = "#cc4b4c";
    } else if ($percent >= 65 && $percent < 75) {
        $color = "#ffcc00";
    } else {
        $color = "#5dbde9";
    }
    
    return $color;
}
//Get Chart color END

//Get Chart color START
function get_chart_label($percent)
{
    if ($percent < 65) {
        $label = "high risk";
    } else if ($percent >= 65 && $percent < 75) {
        $label = "medium risk";
    } else {
        $label = "low risk";
    }
    
    return $label;
}
//Get Chart color END

//Get Categories array for checkboxes START
function get_categories_position() {
    $categoriesPosition = [];
    
    $categories = get_terms( array(
        'taxonomy' => 'question_category',
        'hide_empty' => false,
        'parent' => 0,
        'order_by' => 'term_order',
        'order' => 'DESC'
    ) );
    
    $catPosts = [];
    $totalPosts = 0;
    foreach ($categories as $key => $qCategory) {
        $numPostsInCategory = $qCategory->count;
        $totalPosts += $numPostsInCategory;
        
        $catPosts[$key]['id'] = $qCategory->term_id;
        $catPosts[$key]['name'] = $qCategory->name;
        $catPosts[$key]['num'] = $numPostsInCategory;
    }
    
    $finalPosition = 0;
    foreach ($catPosts as $key => $value) {
        $percentagePosition = ( 100 / $totalPosts ) * $catPosts[$key]['num'];
        $finalPosition += $percentagePosition;
        $catPosts[$key]['position'] = round($finalPosition, 4);
    }
    
    //Remove last checkbox
//    foreach ($catPosts as $key => $catPost) {
//        if ($catPost['position'] == 100) {
//            unset($catPosts[$key]);
//        }
//    }
    
    return $catPosts;
}
//Get Categories array for checkboxes END