<?php
/*
   Plugin Name: Custom RSS Post Importer
   Plugin URI: http://yanev.com
   Description: Custom plugin for RSS importing into posts
   Version: 1.1
   Author: Mile Janev
   Author URI: http://yanev.com
   License: GPL2
*/

//Activation/Deactivation hooks set cron jobs
register_activation_hook(__FILE__, 'rss_activation');
function rss_activation() {
    if (! wp_next_scheduled ('rss_daily_event')) {
	wp_schedule_event(time(), 'daily', 'rss_daily_event');
    }
}
add_action('rss_daily_event', 'rss_feed_populate');

register_deactivation_hook(__FILE__, 'rss_deactivation');
function rss_deactivation() {
    wp_clear_scheduled_hook('rss_daily_event');
}

function rss_init(){
    add_image_size('rss-thumbnail', 96, 96, true);
}
add_action('init', 'rss_init');

//RSS Post START
function rss_post_init() {
    $labels = array(
        'name' => 'RSS Post',
        'singular_name' => 'RSS Post',
        'menu_name' => 'RSS Posts',
        'name_admin_bar' => 'RSS Post',
        'archives' => 'RSS Post Archives',
        'parent_item_colon' => 'Parent RSS Post:',
        'all_items' => 'All RSS Posts',
        'add_new_item' => 'Add New RSS Post',
        'add_new' => 'Add New RSS Post',
        'new_item' => 'New RSS Post',
        'edit_item' => 'Edit RSS Post',
        'update_item' => 'Update RSS Post',
        'view_item' => 'View RSS Post',
        'search_items' => 'Search RSS Posts',
        'not_found' => 'Not found',
        'not_found_in_trash' => 'Not found in Trash',
        'featured_image' => 'Featured Image',
        'set_featured_image' => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image' => 'Use as featured image',
        'insert_into_item' => 'Insert into RSS Post',
        'uploaded_to_this_item' => 'Uploaded to this RSS Post',
        'items_list' => 'RSS Posts list',
        'items_list_navigation' => 'RSS Posts list navigation',
        'filter_items_list' => 'Filter RSS Posts list',
    );
    $args = array(
        'label' => 'RSS Post',
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'rss_post'),
        'query_var' => true,
        'taxonomies' => ['rss_category'],
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
        )
    );
    register_post_type('rss_post', $args);
}
add_action('init', 'rss_post_init');
//RSS Post END

//RSS Category START
function rss_category(){
    //set the name of the taxonomy
    $taxonomy = 'rss_category';
    //set the post types for the taxonomy
    $object_type = 'rss_post';
    
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'RSS Category',
        'singular_name'      => 'RSS Category',
        'search_items'       => 'Search RSS Categories',
        'all_items'          => 'All RSS Categories',
        'parent_item'        => 'Parent RSS Category',
        'parent_item_colon'  => 'Parent RSS Category:',
        'update_item'        => 'Update RSS Category',
        'edit_item'          => 'Edit RSS Category',
        'add_new_item'       => 'Add New RSS Category', 
        'new_item_name'      => 'New RSS Category Name',
        'menu_name'          => 'Categories'
    );
    
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'rss_category')
    );
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init', 'rss_category');
//RSS Category END

//RSS Category Add custom Field START

function rss_category_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[rss_category_link]">
                    <?php _e( 'RSS Category Link', 'ironclad' ); ?>
                </label>
		<input type="text" name="term_meta[rss_category_link]" id="term_meta[rss_category_link]" value="">
		<p class="description">
                    <?php _e( 'Enter a link to RSS field', 'ironclad' ); ?>
                </p>
	</div>
<?php
}
add_action( 'rss_category_add_form_fields', 'rss_category_add_new_meta_field', 10, 2 );

// Edit term page
function rss_category_edit_meta_field($term) {
    // put the term ID into a variable
    $t_id = $term->term_id;

    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option( "taxonomy_$t_id" ); ?>
    <tr class="form-field">
    <th scope="row" valign="top">
        <label for="term_meta[rss_category_link]">
            <?php _e( 'RSS Category Link', 'ironclad' ); ?>
        </label>
    </th>
        <td>
            <input type="text" name="term_meta[rss_category_link]" id="term_meta[rss_category_link]" value="<?php echo esc_attr( $term_meta['rss_category_link'] ) ? esc_attr( $term_meta['rss_category_link'] ) : ''; ?>">
            <p class="description">
                <?php _e( 'Enter a link to RSS field', 'ironclad' ); ?>
            </p>
        </td>
    </tr>
<?php
}
add_action( 'rss_category_edit_form_fields', 'rss_category_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
        foreach ( $cat_keys as $key ) {
            if ( isset ( $_POST['term_meta'][$key] ) ) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        // Save the option array.
        update_option( "taxonomy_$t_id", $term_meta );
    }
}  
add_action( 'edited_rss_category', 'save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_rss_category', 'save_taxonomy_custom_meta', 10, 2 );
//RSS Category Add custom Field END


//Create custom fields for this post type
function rss_fields_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?>
    <div>
        <label for="meta-box-rss-link">Live site link: </label><br />
        <input name="meta-box-rss-link" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-rss-link", true); ?>"  style="width: 100%">
    </div>
    <br />
    <div>
        <label for="meta-box-rss-id">Live site ID: </label><br />
        <input name="meta-box-rss-id" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-rss-id", true); ?>">
    </div>
    <br />
    <div>
        <label for="meta-box-rss-time">Live site publish time: </label><br />
        <input name="meta-box-rss-time" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-rss-time", true); ?>">
    </div>
<?php  
}
function add_rss_meta_box()
{
    add_meta_box("rss-meta-box", "Additional Parameters", "rss_fields_markup", "rss_post", "normal", "high", null);
}
add_action("add_meta_boxes", "add_rss_meta_box");

function save_rss_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;
    
    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
    
    $slug = "rss_post";
    if($slug != $post->post_type) {
        return $post_id;
    }

    if(isset($_POST["meta-box-rss-link"])){
        $meta_box_link_value = $_POST["meta-box-rss-link"];
    } else {
        $meta_box_link_value = "";
    }
    update_post_meta($post_id, "meta-box-rss-link", $meta_box_link_value);
    
    if(isset($_POST["meta-box-rss-id"])){
        $meta_box_id_value = $_POST["meta-box-rss-id"];
    } else {
        $meta_box_id_value = "";
    }
    update_post_meta($post_id, "meta-box-rss-id", $meta_box_id_value);
    
    if(isset($_POST["meta-box-rss-time"])){
        $meta_box_time_value = $_POST["meta-box-rss-time"];
    } else {
        $meta_box_time_value = "";
    }
    update_post_meta($post_id, "meta-box-rss-time", $meta_box_time_value);
}
add_action("save_post", "save_rss_meta_box", 10, 3);

function remove_rss_meta_box()
{
    remove_meta_box("meta-box-rss-link", "rss_post", "normal");
    remove_meta_box("meta-box-rss-id", "rss_post", "normal");
    remove_meta_box("meta-box-rss-time", "rss_post", "normal");
}
add_action("do_meta_boxes", "remove_rss_meta_box");


//RSS field Populate
function rss_feed_populate(){
    
    $terms = get_terms( array(
        'taxonomy' => 'rss_category',
        'hide_empty' => false,
    ) );
    
//    Add All logic here in this foreach
    foreach ($terms as $rss_category) {
        
        $rss_category_link_array = get_option( "taxonomy_".$rss_category->term_id );
        $rss_category_link = $rss_category_link_array['rss_category_link'];
        
        //Replace content to be able to get Image
        $feed = file_get_contents($rss_category_link);
        $feed = str_replace('<media:content', '<postimage', $feed);
        $feed = str_replace('</media:content>', '</postimage>', $feed);

        $xml = new SimpleXMLElement($feed);
        foreach($xml->channel->item as $item) {
            
            //Check is this post already in our DB
            $livePostId = (string)$item->{'post-id'};
            $args = array(
                'post_type' => 'rss_post',
                'meta_query' => array(
                    array(
                        'key' => 'meta-box-rss-id',
                        'value' => $livePostId,
                        'compare' => '=',
                    )
                )
            );
            $postExists = get_posts($args);
            
            if (empty($postExists)) {

                $imageUrl = (string)$item->postimage->attributes()['url'];
                $postDescription = (string)$item->description;
                $publishTime = date("Y-m-d H:i:s", strtotime((string)$item->pubDate));

                $my_post = array(
                    'post_title'    => wp_strip_all_tags($item->title),
                    'post_content'  => $postDescription,
                    'post_type'     => 'rss_post',
                    'post_status'   => 'publish',
                    'post_author'   => 1,
                    'post_date'     => $publishTime
                );
                
                $postID = wp_insert_post($my_post);
                $livePostIdAdded = add_post_meta($postID, "meta-box-rss-id", $livePostId);
                $livePostLinkAdded = add_post_meta($postID, "meta-box-rss-link", (string)$item->link);
                $livePostTimeAdded = add_post_meta($postID, "meta-box-rss-time", $publishTime);
                $categoryAdded = wp_set_object_terms($postID, [$rss_category->term_id], 'rss_category');

                $categoryAdded = wp_set_object_terms($postID, [$rss_category->term_id], 'rss_category');
                
                $uploadImg = uploadImg($imageUrl, $postID);

            } else {
                $postID = $postExists[0]->ID;
                $termsIds = [$rss_category->term_id];
                
                $currentObjectTermsArray = wp_get_object_terms($postID, 'rss_category');
                foreach ($currentObjectTermsArray as $term) {
                    $termsIds[] = $term->term_id;
                }
                $termsIds = array_unique($termsIds);
                if ($termsIds > count($currentObjectTermsArray)) {
                    $categoriesAdded = wp_set_object_terms($postID, $termsIds, 'rss_category');
                }
            }
        }
    }
}
//add_action('init', 'rss_feed_populate');//Remove comment only for testing (will be immediately executed)

function uploadImg($imageUrl = FALSE, $postID = FALSE)
{
    if ($imageUrl) {
        
        //Upload Image START
        $urlArray = explode("/", $imageUrl);
        $filename = $urlArray[count($urlArray)-1];
        
        $uploaddir = wp_upload_dir();
        $uploadfile = $uploaddir['path'] . '/' . $filename;

        $contents= file_get_contents($imageUrl);
        $savefile = fopen($uploadfile, 'w');
        fwrite($savefile, $contents);
        fclose($savefile);
        //Upload Image END
        
        //Insert In DB START
        $wp_filetype = wp_check_filetype(basename($filename), null );
        
        $attachment = array(
            'guid'           => $uploaddir['url'] . '/' . basename( $filename ), 
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => $filename,
            'post_content' => '',
            'post_status' => 'inherit',
        );

        $attach_id = wp_insert_attachment( $attachment, $uploadfile );
        
        $imagenew = get_post( $attach_id );
        $fullsizepath = get_attached_file( $imagenew->ID );
        
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        
        set_post_thumbnail($postID, $imagenew->ID);
        $attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        
        return TRUE;
    } else {
        return FALSE;
    }
}